import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { IWidget } from '../../models/widget.model';
import { WidgetService } from '../../services/widget/widget.service';
import { PipelinesComponent } from '../pipelines/pipelines.component';
import { ProjectswidgetComponent } from '../projectswidget/projectswidget.component';
import { Widget1Component } from '../widget1/widget1.component';
import { Widget2Component } from '../widget2/widget2.component';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit, AfterViewInit {
  @Input() widget!: IWidget
  @ViewChild('widget',{read: ViewContainerRef, static: true})  widgetRef!: ViewContainerRef
  component!: Type<any>
  
  constructor(private ws: WidgetService, private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
    this.render()
  }

  loadWidget(type: string): Type<any>{
    return widgetFactory(type)
  }

  render(){
    if (this.widget.type){
      let wdg
      this.widgetRef.clear()
      wdg = this.widgetRef.createComponent(this.loadWidget(this.widget.type))
      wdg.instance.widget = this.widget
      this.cd.detectChanges();
    }
  }

}


const WidgetType: {[value: string]: Type<any>} = {
	'projects': ProjectswidgetComponent,
	'pipelines': PipelinesComponent,
	'widget1': Widget1Component
}

function widgetFactory(type: string): Type<any> {
    return WidgetType[type]
}
