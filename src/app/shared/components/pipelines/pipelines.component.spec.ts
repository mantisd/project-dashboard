import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectswidgetComponent } from './pipelines.component';

describe('ProjectswidgetComponent', () => {
  let component: ProjectswidgetComponent;
  let fixture: ComponentFixture<ProjectswidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectswidgetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectswidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
