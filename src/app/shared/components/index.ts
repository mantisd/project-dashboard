import { WidgetComponent } from "./widget/widget.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { PipelinesComponent } from "./pipelines/pipelines.component";

export const CMPNTS = [
    SidebarComponent,
    ToolbarComponent,
    WidgetComponent,
    PipelinesComponent
]