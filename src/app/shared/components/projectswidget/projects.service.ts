import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ProjectsService {
  activeProject$ = new Subject<Project>()
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  getProjects():Observable<Project[]> {
    return this.http.get<Project[]>(`${environment.gitlabBaseUrl}groups/${environment.groupId}/projects?per_page=10`,this.httpOptions)
  }
  setActiveProject(project: Project){
    this.activeProject$.next(project)
  }
}

export interface Project {
  name?: boolean
  id?: string
  archived: boolean
  id_after: number
  id_before: number
  imported: boolean
  last_activity_after: Date
  last_activity_before: Date
  membership: boolean
  min_access_level: number
  order_by: string
  owned: boolean
  repository_checksum_failed: boolean
  repository_storage: string
  search_namespaces: boolean
  search: string
  simple: boolean
  sort: string
  starred: boolean
  statistics: boolean
  topic: string
  topic_id: number
  visibility: string
  wiki_checksum_failed: boolean
  with_custom_attributes: boolean
  with_issues_enabled: boolean
  with_merge_requests_enabled: boolean
  with_programming_language: string
}