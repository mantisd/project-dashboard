export interface IWidget {
    name: string,
    type: string,
    id: string,
    label: string,
    visible: boolean
    options?: any 
}