import { Component, OnInit } from '@angular/core';
import { IWidget } from '../../models/widget.model';

@Component({
  selector: 'app-widget1',
  templateUrl: './widget1.component.html',
  styleUrls: ['./widget1.component.scss']
})
export class Widget1Component implements OnInit {
  widget!: IWidget
  constructor() { }

  ngOnInit(): void {
  }

}
