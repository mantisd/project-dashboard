import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IWidget } from '../../models/widget.model';

@Injectable({
  providedIn: 'root'
})
export class WidgetService {
  widgets$ = new BehaviorSubject<IWidget[]>([
    {
      name: 'widget1',
      type: 'projects',
      id: 'w-00',
      label: 'widget1',
      visible: true
    },
    {
      name: 'widget2',
      type: 'pipelines',
      id: 'w-01',
      label: 'widget2',
      visible: false
    }
  ])
  constructor() { }
}
 
