import { Component, ElementRef, ViewChild } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from './core/services/user.service';
import { Project, ProjectsService } from './shared/components/projectswidget/projects.service';
import { IWidget } from './shared/models/widget.model';
import { WidgetService } from './shared/services/widget/widget.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gitlab-dashboard';
  @ViewChild('sidenav', { read: ElementRef, static: true }) sidenav!: ElementRef;
  user$!: Observable<string>
  token$!: Observable<string>
  widgets$!: Observable<IWidget[]>
  activeProject$: Subject<Project> = this.ps.activeProject$
  menuOpen= false
  constructor(
    private us: UserService, 
    private ws: WidgetService, 
    private ps: ProjectsService
    ) { }

  ngOnInit(): void {
    this.user$ = this.us.username$
    this.token$ = this.us.token$
    this.widgets$ = this.ws.widgets$
    
  }



  toggleMenu() {
    this.menuOpen = !this.menuOpen
    this.sidenav.nativeElement.style.width = this.sidenav.nativeElement.style.width === environment.sidenav.width ? '75px' : environment.sidenav.width;
  }

  logout(){
    this.us.logout()
    this.ps.activeProject$.next({
      archived: false,
      id_after: 0,
      id_before: 0,
      imported: false,
      last_activity_after: new Date(),
      last_activity_before:  new Date(),
      membership: false,
      min_access_level: 0,
      order_by: '',
      owned: false,
      repository_checksum_failed: false,
      repository_storage: '',
      search_namespaces: false,
      search: '',
      simple: false,
      sort: '',
      starred: false,
      statistics: false,
      topic: '',
      topic_id: 0,
      visibility: '',
      wiki_checksum_failed: false,
      with_custom_attributes: false,
      with_issues_enabled: false,
      with_merge_requests_enabled: false,
      with_programming_language: ''
    })
  }
}
