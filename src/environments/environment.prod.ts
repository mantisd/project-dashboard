export const environment = {
  production: true,
  gitlabBaseUrl: 'https://gitlab.com/api/v4/',
  groupId: '57358070',
  sidenav: {
    width: '300px',
    mode: 'side'
  }
};
