import { Provider } from "@angular/core";
import { AuthGuard } from "./auth.guard";

export const GUARDS: Provider[] = [
    AuthGuard
]