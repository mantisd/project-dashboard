import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  hide = true;
  submitted = false;

  values = {
    username: '',
    token: "" 
  }

  constructor(
    private us:UserService,
    private rs: Router
    ) { }

  ngOnInit(): void {
    this.checkAuthSet()
  }

  checkAuthSet(){
    this.us.isLoggedIn$.pipe(take(1)).subscribe((auth:boolean)=>{
      auth ? this.rs.navigateByUrl('dashboard') : console.log('User Not Set!');
    })
  }
 

  onSubmit() { 
    this.submitted = true; 
    this.us.setUsername(this.values.username);
    this.us.setToken(this.values.token)
    this.rs.navigateByUrl('dashboard')
  }

}
