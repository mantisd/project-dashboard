import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable, take, takeLast } from 'rxjs';
import { Project, ProjectsService } from './projects.service';

@Component({
  selector: 'app-projectswidget',
  templateUrl: './projectswidget.component.html',
  styleUrls: ['./projectswidget.component.scss'],
})
export class ProjectswidgetComponent implements OnInit {
  projects$ = this.ps.getProjects();
  constructor(private ps: ProjectsService, private router:Router) { 
  }
  ngOnInit(): void {
    this.load()
  }
  displayedColumns: string[] = ['id', 'name', 'visibility', 'owned'];
  dataSource!: MatTableDataSource<Project>;

  @ViewChild('MatPaginator') paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  load(){
    this.ps.getProjects().pipe(take(1)).subscribe((projects: Project[])=>{
      this.dataSource = new MatTableDataSource(projects);
    })
    
  }

  onNavToProject(project: Project){
    this.ps.setActiveProject(project)
    this.router.navigate(['project',project.id])
  }
}
