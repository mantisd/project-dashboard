import { AfterContentInit, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';
import { Pipeline, PipelinesService } from './pipelines.service';

@Component({
  selector: 'app-pipelines',
  templateUrl: './pipelines.component.html',
  styleUrls: ['./pipelines.component.scss'],
  providers: [
    PipelinesService
  ]
})
export class PipelinesComponent implements OnInit,AfterViewInit {
  projectId!:string
  pipelines$ = this.ps.getPipelines(this.projectId);
  constructor(private ps: PipelinesService, private rs: ActivatedRoute) { 
  }
  ngOnInit(): void {
    this.projectId = this.rs.snapshot.params['id']
    this.load()
  }
  displayedColumns: string[] = ['id', 'ref', 'created_at', 'web_url'];
  dataSource!: MatTableDataSource<Pipeline>;

  @ViewChild('MatPaginator') paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
      
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  load(){
    this.ps.getPipelines(this.projectId).pipe(take(1)).subscribe((pipelines: Pipeline[])=>{
      this.dataSource = new MatTableDataSource(pipelines);
    })
    
  }
}
