FROM node:alpine  as build 

WORKDIR /build 

RUN npm i -g @angular/cli

RUN ng config -g cli.warnings.versionMismatch false

COPY ./package*.json /build/

RUN npm i 

COPY . .

RUN ng build 


FROM lscr.io/linuxserver/nginx:latest as prod

ENV PUID=1000
ENV PGID=1000
ENV TZ=America/Phoenix

EXPOSE 443
EXPOSE 80

COPY --from=build /build/dist/gitlab-dashboard /config/www
