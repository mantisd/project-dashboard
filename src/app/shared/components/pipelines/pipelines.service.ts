import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class PipelinesService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  getPipelines(projectId:string): Observable<Pipeline[]> {
    return this.http.get<Pipeline[]>(`${environment.gitlabBaseUrl}projects/${projectId}/pipelines?per_page=10`, this.httpOptions)
  }
}

export interface Pipeline {
  id: string
  iid: string,
  project_id: string,
  status: string,
  ref: string
  sha: string,
  before_sha: string,
  created_at: string,
  updated_at: string,
  web_url: string
}


