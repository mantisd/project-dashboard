import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  hide = true;
  submitted = false;

  user$ = this.us.username$
  token$ = this.us.token$
  
  values = {
    username: '',
    token: "" 
  }

  constructor(
    private us:UserService,
    ) { }

  ngOnInit(): void {
    this.user$.pipe(take(1)).subscribe((name)=>{
      this.values.username = name
    })
    this.token$.pipe(take(1)).subscribe((token)=>{
      this.values.token = token
    })
  }


  onSubmit() { 
    this.submitted = true; 
    this.us.setUsername(this.values.username);
    this.us.setToken(this.values.token)
  }

}
