import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService {
  username$ = new BehaviorSubject<string>('')
  token$ = new BehaviorSubject<string>('')
  isLoggedIn$ = new BehaviorSubject<boolean>(true)

  constructor(private router: Router) { 
    this._checkCache();
    this._checkAuth();
  }

  private _getStoredToken() {
   if (this.tokenExists){
    return localStorage.getItem('Gitlab_Token')
   }
   else {
    return null
   }
  }
  private _getStoredUsername(): string | null {
    if (this.usernameExists) { 
      return localStorage.getItem('Gitlab_Username')
    } else { 
      return null
    }
  }
  
  get usernameExists() {
    const username = localStorage.getItem('Gitlab_Username')
    if (username) { 
      return true 
    } else { 
      return false
    }
  }
  get tokenExists() {
    const username = localStorage.getItem('Gitlab_Token')
    if (username) { 
      return true 
    } else { 
      return false
    }
  }

  private _checkCache(){
    const user = localStorage.getItem('Gitlab_Username')
    const token = localStorage.getItem('Gitlab_Token')
    this.username$.next(user ? user : '')
    this.token$.next(token ? token : '')
  }

  private _checkAuth(){
    if(this.usernameExists && this.tokenExists) {
      this.setLoggedIn()
    }
    else {
      this.setLoggedOut()
    }
  }

  setToken(token: string) {
    localStorage.setItem('Gitlab_Token',token)
    this.token$.next(token)
    this._checkAuth()
  }
  setUsername(username: string) {
    localStorage.setItem('Gitlab_Username',username)
    this.username$.next(username)
    this._checkAuth()
  }

 

  setLoggedOut(){
    this.isLoggedIn$.next(false)
  }

  setLoggedIn(){
    this.isLoggedIn$.next(true)
  }

  logout(){
    localStorage.removeItem('Gitlab_Token')
    localStorage.removeItem('Gitlab_Username')
    this.setLoggedOut();
    this.router.navigate(['home'])
  }

}
