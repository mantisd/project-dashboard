import { Component, OnInit } from '@angular/core';
import { IWidget } from '../../models/widget.model';

@Component({
  selector: 'app-widget2',
  templateUrl: './widget2.component.html',
  styleUrls: ['./widget2.component.scss']
})
export class Widget2Component implements OnInit {
  widget!: IWidget
  constructor() { }

  ngOnInit(): void {
  }

}
