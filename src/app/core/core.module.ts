import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { SVCS } from './services';
import { HttpClientModule } from '@angular/common/http';
import { INTERCEPTORS } from './interceptors';
import { GUARDS } from './guards';


@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [
    FormsModule
  ],
  providers: [
    SVCS,
    GUARDS,
    INTERCEPTORS
  ]
})
export class CoreModule { }
