import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SVC } from './services';
import { DRCTV } from './directives';
import { CMPNTS } from './components';
import { MATERIAL_CMPNTS } from './@material';
import { ChangeWidthDirective } from './directives/change-width.directive';
import { WidgetComponent } from './components/widget/widget.component';
import { Widget1Component } from './components/widget1/widget1.component';
import { Widget2Component } from './components/widget2/widget2.component';
import { ProjectswidgetComponent } from './components/projectswidget/projectswidget.component';



@NgModule({
  declarations: [DRCTV, CMPNTS, ChangeWidthDirective, Widget1Component, Widget2Component, ProjectswidgetComponent],
  imports: [
    CommonModule,
    MATERIAL_CMPNTS
  ],
  providers: [SVC],
  exports: [ DRCTV, CMPNTS, MATERIAL_CMPNTS]
})
export class SharedModule { }
