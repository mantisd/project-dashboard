import { Routes } from "@angular/router";
import { HomeComponent } from "../pages/home/home.component";
import { SettingsComponent } from "../pages/settings/settings.component";
import { DashboardComponent } from "../pages/dashboard/dashboard.component";
import { PipelinesComponent } from "../shared/components/pipelines/pipelines.component";
import { AuthGuard } from "../core/guards/auth.guard";

export const ROUTES: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, title: 'Home' },
    { path: 'dashboard', component: DashboardComponent, title: 'Dashboard', canActivate: [AuthGuard] },
    { path:'project/:id', component: PipelinesComponent, canActivate: [AuthGuard] },
    { path: 'settings', component: SettingsComponent, title: 'Settings', canActivate: [AuthGuard]  }
]