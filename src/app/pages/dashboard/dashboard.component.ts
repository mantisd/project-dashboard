import { ThisReceiver } from '@angular/compiler';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/core/services/user.service';
import { IWidget } from 'src/app/shared/models/widget.model';
import { WidgetService } from 'src/app/shared/services/widget/widget.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav',{read: ElementRef, static: true}) sidenav!: ElementRef;
  user$!: Observable<string>
  token$!: Observable<string>
  widgets$!: Observable<IWidget[]>
  constructor(private us: UserService, private ws: WidgetService, private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.user$ = this.us.username$
    this.token$ = this.us.token$
    this.widgets$ = this.ws.widgets$
  }

  ngAfterViewInit(){
    // this.cd.detectChanges()
  }

  toggleMenu(){
    this.sidenav.nativeElement.style.width = this.sidenav.nativeElement.style.width  === environment.sidenav.width ? '75px': environment.sidenav.width ;
  }

}
